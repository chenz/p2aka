#ifndef EMULINO_H_INCLUDED
#define EMULINO_H_INCLUDED

#include <stdint.h>

#if defined(NO_ARDUINO)

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

enum LogicLevel {
    LOW = 0,
    HIGH
};

enum PinMode {
    INPUT_PULLUP,
    OUTPUT,
};

#define delay _delay_ms
#define delayMicroseconds _delay_us

uint32_t millis();
LogicLevel digitalRead(const uint8_t bit);
void digitalWrite(const uint8_t bit, const LogicLevel level);
void pinMode(const uint8_t bit, const PinMode mode);

namespace emulino
{
    void setup();

    struct ScopedInterruptDisabler
    {
        ScopedInterruptDisabler();
        ~ScopedInterruptDisabler();
        ScopedInterruptDisabler(ScopedInterruptDisabler&) = delete;
        ScopedInterruptDisabler& operator=(ScopedInterruptDisabler&) = delete;
        uint8_t sreg;
    };
}

#else // defined(NO_EMULINO)

#include <Arduino.h>

#endif // defined(NO_EMULINO)

#endif // EMULINO_H_INCLUDED
