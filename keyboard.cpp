#include "keyboard.h"

namespace p2aka
{
    Keyboard keyboard;

    void Keyboard::isr(void)
    {
        static uint8_t bit_count = 0;
        static uint8_t byte = 0;
        static uint32_t prev_ms = 0;

        const auto bit = digitalRead(DATA_PIN);
        const auto now_ms = millis();
        if( now_ms - prev_ms > 250 ) {
            bit_count = 0;
            byte = 0;
        }
        prev_ms = now_ms;
        const uint8_t pos = bit_count - 1;
        if( pos <= 7 ) {
            byte |= (bit << pos);
        }
        bit_count++;
        if( bit_count == 11 ) {
            const auto new_head = (keyboard.buffer_head + 1) % BUFFER_SIZE;
            if( new_head != keyboard.buffer_tail ) {
                keyboard.buffer[keyboard.buffer_head] = byte;
                keyboard.buffer_head = new_head;
            }
            bit_count = 0;
            byte = 0;
        }
    }

    void Keyboard::init()
    {
        pinMode(CLOCK_PIN, INPUT_PULLUP);
        pinMode(DATA_PIN, INPUT_PULLUP);
        #if defined(NO_ARDUINO)
        // enable INT0 on falling edge
        MCUCR = (MCUCR & 0xFC) | (1 << ISC01);
        GIMSK |= (1 << INT0);
        #else
        attachInterrupt(0, isr, FALLING); // FIXME: hard coded interrupt
        #endif
    }

    uint8_t Keyboard::next_scancode()
    {
        if( buffer_head == buffer_tail ) {
            return 0;
        }
        const auto code = buffer[buffer_tail];
        buffer_tail = (buffer_tail + 1) % BUFFER_SIZE;
        return code;
    }

} // namespace

#if defined(NO_ARDUINO)
ISR(INT0_vect)
{
    p2aka::Keyboard::isr();
}
#endif
