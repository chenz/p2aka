CXXFLAGS = -Wall -W -O2 -mmcu=attiny85 -std=c++11 -DF_CPU=8000000UL -DNO_ARDUINO

CXXFLAGS += -DCFG_KEYBOARD_CLOCK_PIN=PB2
CXXFLAGS += -DCFG_KEYBOARD_DATA_PIN=PB1
CXXFLAGS += -DCFG_AMIGA_KCLK_PIN=PB3
CXXFLAGS += -DCFG_AMIGA_KDAT_PIN=PB4

SOURCES = main.cpp amiga.cpp keyboard.cpp emulino.cpp
OBJECTS = $(SOURCES:.cpp=.o)

CXX = avr-g++
OBJCOPY = avr-objcopy
TARGET = p2aka

LDFLAGS = -mmcu=attiny85 -Wl,--gc-sections

depend-all: depend
	$(MAKE) -f tiny.mk all

depend: $(SOURCES)
	$(CXX) $(CXXFLAGS) -M $(SOURCES) > .depend

all: $(TARGET).hex

clean:
	$(RM) $(TARGET).hex $(TARGET).elf $(OBJECTS)

$(TARGET).elf: $(OBJECTS)
	$(CXX) $(LDFLAGS) -o $@ $(OBJECTS)

%.hex: %.elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

$(OBJECTS): tiny.mk

.PHONY: depend-all depend all clean

-include .depend
