#include "amiga.h"
#include "emulino.h"
#include "keyboard.h"

void setup()
{
    delay(1000);
    #if defined(SERIAL_DEBUG)
    Serial.begin(9600);
    while(!Serial);
    Serial.println("begin...");
    #endif
    p2aka::keyboard.init();
    amiga.init();
}

void loop()
{
    amiga.update();
}

#if defined(NO_ARDUINO)

int main()
{
    emulino::setup();
    setup();
    sei();
    for(;;) loop();
    return 0;
}

#endif
