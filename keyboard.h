#ifndef KEYBOARD_H_INCLUDED
#define KEYBOARD_H_INCLUDED

#include "emulino.h"

namespace p2aka
{
    struct Keyboard
    {
        static constexpr uint8_t CLOCK_PIN = CFG_KEYBOARD_CLOCK_PIN;
        static constexpr uint8_t DATA_PIN = CFG_KEYBOARD_DATA_PIN;
        static constexpr uint8_t BUFFER_SIZE = 32;

        static void isr();

        void init();
        uint8_t next_scancode();

        volatile uint8_t buffer[BUFFER_SIZE];
        volatile uint8_t buffer_head = 0, buffer_tail = 0;
    };

    extern Keyboard keyboard;
}

#endif /* KEYBOARD_H_INCLUDED */
