p2aka
=====

This is a PC to Amiga keyboard adapter implemented in an AVR
microcontroller. I made this project in order to use an AT keyboard on
an Amiga 2000 without any external adapters.

The code was first developed on an Arduino Micro (using Arduino.mk),
and then ported to the ATtiny85. A minimal compatibility layer
("emulino") was written to keep the code (in theory) working on the
Arduino - this has not been tested lately though.

The project is currently in a "works-for-me" state an not being
actively developed - see Status/Improvements below for more info.

Copyright (c) 2016-2018 Christian Henz, published under the GNU
General Public License, see LICENSE.txt for details.

Building
--------

The code is written in C++11 and requires GCC >= 4.9 to build. The AVR
toolchain can easily be installed on Debian/Ubuntu-like systems
(Packages `gcc-avr`, `avr-libc`). Also requires GNU make.

To build the ATtiny85 version:

    make -f tiny.mk

For the (untested) Arduino version, install Arduino.mk (Debian/Ubuntu
package `arduino-mk`, version 1.5.2,
https://github.com/sudar/Arduino-Makefile) and use `Makefile` instead.

See the respective makefiles for configuration options.

Hardware
--------

See tiny.mk or Makefile for the pin configurations. 

Runs of the built-in 8MHz RC-Oscillator. Requires no external
components except for maybe some decoupling on the power rail and two
resistors for in-system programming - YMMV (consult app note AVR042).

Status/Improvements
-------------------

- Features atm a hard-coded 102 keyboard layout table, with a couple
  of special cases handled in code. This should be generalized to a
  completely data-driven approach, so the layout can more easily be
  customized.
  
- Compile-time-only configuration/customization at the moment, so any
  change requires re-programming the uC (this should be possible
  in-system though). "Online" configuration could be implemented using
  special key combos (settings would be stored in the built-in
  EEPROM).

- Currently, the single layout table is put into RAM (init'ed by the
  C++ runtime) - in order to fit multiple layouts, these tables would
  have to be put into PROGMEM and copied into RAM "manually" for
  customization.
  
- Must currently run at 8MHz, because at 1MHz, the interrupt handler
  was not able to keep up with the PS/2 clock. It may be possible to
  run at 1MHz after rewriting the ISR in assembly.
