#include "amiga.h"
#include "keyboard.h"

enum AmigaScancode {
    SCANCODE_CTRL    = 0x63,
    SCANCODE_L_AMIGA = 0x66,
    SCANCODE_R_AMIGA = 0x67,
    SCANCODE_NONE    = 0xFF
};

Amiga amiga;

//enum StatusCode
//{
//    RESET_WARNING = 0x78,
//    LOST_SYNC = 0xF9,
//    OUTPUT_BUFFER_OVERFLOW = 0xFA,
//    SELF_TEST_FAILED = 0xFC,
//    INITIATE_POWER_UP_KEY_STREAM = 0xFD,
//    TERMINATE_POWER_UP_KEY_STREAM = 0xFE,
//};

Amiga::Amiga()
{
    state_func = &Amiga::sync_state;
}

void Amiga::init()
{
    digitalWrite(KCLK_PIN, HIGH);
    pinMode(KCLK_PIN, OUTPUT);
    pinMode(KDAT_PIN, INPUT_PULLUP);
}

void Amiga::update()
{
    (this->*state_func)();
}

void Amiga::setup_ack(StateFunc on_ack, StateFunc on_no_ack)
{
    ack_start_ms = millis();
    ack_state_func = on_ack;
    no_ack_state_func = on_no_ack;
    state_func = &Amiga::await_ack_state;
}

void Amiga::await_ack_state()
{
    if( !digitalRead(KDAT_PIN) ) {
        state_func = &Amiga::await_end_ack_state;
    }
    else if( millis() - ack_start_ms >= ACK_TIMEOUT_MS ) {
        state_func = no_ack_state_func;
    }
}

void Amiga::await_end_ack_state()
{
    // TODO: timeout?
    if( digitalRead(KDAT_PIN) ) {
        state_func = ack_state_func;
    }
}

void Amiga::sync_state()
{
    pinMode(KDAT_PIN, OUTPUT);
    digitalWrite(KDAT_PIN, LOW); // active low
    delayMicroseconds(20);
    digitalWrite(KCLK_PIN, LOW);
    delayMicroseconds(20);
    digitalWrite(KCLK_PIN, HIGH);
    delayMicroseconds(20);
    pinMode(KDAT_PIN, INPUT_PULLUP);
    setup_ack(&Amiga::await_keycode_state, &Amiga::sync_state);
}

void Amiga::await_keycode_state()
{
    const auto code = map_next_scancode();
    if( code != SCANCODE_NONE ) {
        send_byte(code);
        setup_ack(is_caa() ? &Amiga::reset_warning_state : &Amiga::await_keycode_state,
                  &Amiga::sync_state);
    }
}

void Amiga::reset_warning_state()
{
    send_byte(0x78);
    setup_ack(&Amiga::second_reset_warning_state, &Amiga::hard_reset_state);
}

void Amiga::second_reset_warning_state()
{
    send_byte(0x78);
    setup_ack(&Amiga::hard_reset_state, &Amiga::sync_state);
}

void Amiga::hard_reset_state()
{
    digitalWrite(KCLK_PIN, LOW);
    const auto reset_start_ms = millis();
    while( is_caa() || millis() - reset_start_ms < 550 ) {
        map_next_scancode();
    }
    digitalWrite(KCLK_PIN, HIGH);
    state_func = &Amiga::sync_state;
}

void Amiga::send_bit(const uint8_t byte, const uint8_t pos)
{
    digitalWrite(KDAT_PIN, ((byte >> pos) & 1) ? LOW : HIGH); // active low
    delayMicroseconds(20);
    digitalWrite(KCLK_PIN, LOW);
    delayMicroseconds(20);
    digitalWrite(KCLK_PIN, HIGH);
    delayMicroseconds(20);
}

void Amiga::send_byte(const uint8_t byte)
{
    pinMode(KDAT_PIN, OUTPUT);
    send_bit(byte, 6);
    send_bit(byte, 5);
    send_bit(byte, 4);
    send_bit(byte, 3);
    send_bit(byte, 2);
    send_bit(byte, 1);
    send_bit(byte, 0);
    send_bit(byte, 7);
    pinMode(KDAT_PIN, INPUT_PULLUP);
}

uint8_t pc102[0x84] = {
  SCANCODE_NONE
  , 0x58 // 0x01, F9
  , SCANCODE_NONE
  , 0x54 // 0x03, F5
  , 0x52 // 0x04, F3
  , 0x50 // 0x05, F1
  , 0x51 // 0x06, F2
  , SCANCODE_NONE // 0x07, F12
  , SCANCODE_NONE
  , 0x59 // 0x09, F10
  , 0x57 // 0x0A, F8
  , 0x55 // 0x0B, F6
  , 0x53 // 0x0C, F4
  , 0x42 // 0x0D, TAB
  , SCANCODE_NONE // 0x0E, `
  , SCANCODE_NONE
  , SCANCODE_NONE
  , SCANCODE_L_AMIGA // 0x11, L ALT  --> L AMIGA
  , 0x60 // 0x12, L SHFT
  , SCANCODE_NONE
  , 0x64 // 0x14, L CTRL --> L ALT
  , 0x10 // 0x15, Q
  , 0x01 // 0x16, 1
  , SCANCODE_NONE
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x31 // 0x1A, Z
  , 0x21 // 0x1B, S
  , 0x20 // 0x1C, A
  , 0x11 // 0x1D, W
  , 0x02 // 0x1E, 2
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x33 // 0x21, C
  , 0x32 // 0x22, X
  , 0x22 // 0x23, D
  , 0x12 // 0x24, E
  , 0x04 // 0x25, 4
  , 0x03 // 0x26, 3
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x40 // 0x29, SPACE
  , 0x34 // 0x2A, V
  , 0x23 // 0x2B, F
  , 0x14 // 0x2C, T
  , 0x13 // 0x2D, R
  , 0x05 // 0x2E, 5
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x36 // 0x31, N
  , 0x35 // 0x32, B
  , 0x25 // 0x33, H
  , 0x24 // 0x34, G
  , 0x15 // 0x35, Y
  , 0x06 // 0x36, 6
  , SCANCODE_NONE
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x37 // 0x3A, M
  , 0x26 // 0x3B, J
  , 0x16 // 0x3C, U
  , 0x07 // 0x3D, 7
  , 0x08 // 0x3E, 8
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x38 // 0x41, ','
  , 0x27 // 0x42, K
  , 0x17 // 0x43, I
  , 0x18 // 0x44, O
  , 0x0A // 0x45, 0
  , 0x09 // 0x46, 9
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x39 // 0x49, .
  , 0x3A // 0x4A, /
  , 0x28 // 0x4B, L
  , 0x2A // 0x4C, ;
  , 0x19 // 0x4D, P
  , 0x0B // 0x4E, -
  , SCANCODE_NONE
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x2A // 0x52, '
  , SCANCODE_NONE
  , 0x1A // 0x54, [
  , 0x0C // 0x55, =
  , SCANCODE_NONE
  , SCANCODE_NONE
  , SCANCODE_CTRL // 0x58, CAPS --> CTRL
  , 0x61 // 0x59, R SHFT
  , 0x44 // 0x5A, ENTER
  , 0x1B // 0x5B, ]
  , SCANCODE_NONE
  , 0x0D // 0x5D, '\'
  , SCANCODE_NONE
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x30 // INT1, '<'
  , SCANCODE_NONE
  , SCANCODE_NONE
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x41 // 0x66, BKSP
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x1D // 0x69, KP 1
  , SCANCODE_NONE
  , 0x2D // 0x6B, KP 4
  , 0x3D // 0x6C, KP 7
  , SCANCODE_NONE
  , SCANCODE_NONE
  , SCANCODE_NONE
  , 0x0F // 0x70, KP 0
  , 0x3C // 0x71, KP .
  , 0x1E // 0x72, KP 2
  , 0x2E // 0x73, KP 5
  , 0x2F // 0x74, KP 6
  , 0x3E // 0x75, KP 8
  , 0x45 // 0x76, ESC
  , SCANCODE_NONE // 0x77, NUM
  , SCANCODE_NONE // 0x78, F11
  , 0x5E // 0x79, KP +
  , 0x1F // 0x7A, KP 3
  , 0x4A // 0x7B, KP -
  , 0x5D // 0x7C, KP *
  , 0x3F // 0x7D, KP 9
  , SCANCODE_NONE // 0x7E, SCROLL
  , SCANCODE_NONE //
  , SCANCODE_NONE //
  , SCANCODE_NONE //
  , SCANCODE_NONE //
  , 0x56 // 0x83, F7
};

uint8_t Amiga::map_next_scancode()
{
    for(;;) {
        const auto code = p2aka::keyboard.next_scancode();
        if( !code ) {
            return SCANCODE_NONE;
        }
        else if( code == 0xF0 ) {
            #if defined(SERIAL_DEBUG)
            Serial.println("B");
            #endif
            state_flags |= BREAK;
        }
        else if( code == 0xE0 ) {
            #if defined(SERIAL_DEBUG)
            Serial.println("E");
            #endif
            state_flags |= EXTENDED;
        }
        else {
            uint8_t mapped = SCANCODE_NONE;
            if( state_flags & EXTENDED ) {
                // TODO: hard coded mapping with PC102 layout tweaks...
                switch( code ) {
                case 0x14: // R CTRL
                    mapped = 0x64; // --> R ALT
                    break;
                case 0x11: // R ALT
                    mapped = SCANCODE_R_AMIGA; // --> R AMIGA
                    break;
                case 0x71: // DEL
                    mapped = 0x46;
                    break;
                case 0x75: // U ARROW
                    mapped = 0x4C;
                    break;
                case 0x6B: // L ARROW
                    mapped = 0x4F;
                    break;
                case 0x72: // D ARROW
                    mapped = 0x4D;
                    break;
                case 0x74: // R ARROW
                    mapped = 0x4E;
                    break;
                case 0x4A: // KP ENTER
                    mapped = 0x5C;
                    break;
                case 0x5A: // KP ENTER
                    mapped = 0x43;
                    break;
                }
            }
            else if( code < 0x84 ) {
                mapped = pc102[code];
            }
            #if defined(SERIAL_DEBUG)
            Serial.println(mapped, HEX);
            #endif
            if( mapped != SCANCODE_NONE ) {
                const auto is_break = state_flags & BREAK;
                auto check_reset = [&](const uint8_t scancode, const uint8_t mask) {
                    if( mapped == scancode ) {
                        if( is_break ) {
                            state_flags &= ~mask;
                        }
                        else {
                            state_flags |= mask;
                        }
                    }
                };
                check_reset(SCANCODE_CTRL, CTRL);
                check_reset(SCANCODE_L_AMIGA, L_AMIGA);
                check_reset(SCANCODE_R_AMIGA, R_AMIGA);
                mapped |= (is_break ? 0x80 : 0x00);
            }
            state_flags &= ~(BREAK | EXTENDED);
            return mapped;
        }
    }
}
