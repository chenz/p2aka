#include "emulino.h"
#include "keyboard.h"

#if defined(NO_ARDUINO)

namespace emulino
{
    volatile uint32_t ticks_ms = 0;

    void setup()
    {
        const auto DIV = 256.0;
        const auto F_TIMER = F_CPU / DIV;
        const auto F_INTERRUPT = 200.0; // 200Hz = 5ms steps
        const auto COMPVAL_ = F_TIMER / F_INTERRUPT;
        static_assert( COMPVAL_ > 0, "" );
        static_assert( COMPVAL_ <= 256, "" );
        const uint8_t COMPVAL = COMPVAL_ - 1;

        OCR1C = COMPVAL;
        OCR1A = COMPVAL;
        TCCR1 = (1 << CTC1) | 0x9; // reset on comparator match, CK/256
        TIMSK = (1 << OCIE1A); // enable interrupt
    }

    ScopedInterruptDisabler::ScopedInterruptDisabler()
        : sreg(SREG)
    {
        cli();
    }

    ScopedInterruptDisabler::~ScopedInterruptDisabler()
    {
        SREG = sreg;
    }
}

using namespace emulino;

ISR(TIM1_COMPA_vect)
{
    ticks_ms += 5;
}

uint32_t millis()
{
    const ScopedInterruptDisabler sid;
    return ticks_ms;
}

void digitalWrite(const uint8_t bit, const LogicLevel level)
{
    if( level == HIGH ) {
        PORTB |= (1 << bit);
    }
    else {
        PORTB &= ~(1 << bit);
    }
}

LogicLevel digitalRead(const uint8_t bit)
{
    return (PINB & (1 << bit)) ? HIGH : LOW;
}

void pinMode(const uint8_t bit, const PinMode mode)
{
    switch( mode ) {
    case OUTPUT:
        {
            const ScopedInterruptDisabler sid;
            DDRB |= (1 << bit);
        }
        break;
    case INPUT_PULLUP:
        {
            const ScopedInterruptDisabler sid;
            DDRB &= ~(1 << bit);
            PORTB |= (1 << bit);
        }
        break;
    }
}

#endif
