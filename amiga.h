#ifndef AMIGA_H_INCLUDED
#define AMIGA_H_INCLUDED

#include "emulino.h"

// State machine for handling the Amiga side
struct Amiga
{
    static constexpr uint32_t ACK_TIMEOUT_MS = 143;

    static constexpr int KCLK_PIN = CFG_AMIGA_KCLK_PIN;
    static constexpr int KDAT_PIN = CFG_AMIGA_KDAT_PIN;

    enum {
        EXTENDED = 0x01,
        BREAK    = 0x02,
        L_AMIGA  = 0x04,
        R_AMIGA  = 0x08,
        CTRL     = 0x10,
    };

    using StateFunc = void (Amiga::*)();

    Amiga();

    void init();
    void update();

    void setup_ack(StateFunc ack_func, StateFunc no_ack_func);
    void await_ack_state();
    void await_end_ack_state();
    void sync_state();
    void await_keycode_state();
    void reset_warning_state();
    void second_reset_warning_state();
    void hard_reset_state();

    void send_bit(const uint8_t byte, const uint8_t pos);
    void send_byte(const uint8_t byte);

    uint8_t map_next_scancode();
    bool is_caa() const { return (state_flags & (CTRL | L_AMIGA | R_AMIGA)) == (CTRL | L_AMIGA | R_AMIGA); }

    StateFunc state_func = nullptr;
    StateFunc ack_state_func = nullptr;
    StateFunc no_ack_state_func = nullptr;

    uint32_t ack_start_ms;

    uint8_t state_flags = 0;
};

extern Amiga amiga;

#endif /* AMIGA_H_INCLUDED */
